function total() {
    let sub1 = parseInt(document.getElementById("eng").value);
    let sub2 = parseInt(document.getElementById("bng").value);
    let sub3 = parseInt(document.getElementById("math").value);
    let sub4 = parseInt(document.getElementById("com").value);

    if (sub1>100 || sub2>100 || sub3>100 || sub4>100 ) {
        alert("Please Enter mark in range of 100");
    }
    
    else{
        let total = sub1 + sub2 + sub3 + sub4;
        document.getElementById("total").innerHTML = "<b>English : <b>" + " " + sub1 + "<br><b> Bangla :<b>" + " " + sub2 + "<br><b> Math :<b>" + " " + sub3 + "<br><b>Computer :<b>"+ " " + sub4 + "<br><br>  <b><hr>Total Marks :<b>" + "  " + total ;
    }
}

function Average() {

    let sub1 = parseInt(document.getElementById("eng").value);
    let sub2 = parseInt(document.getElementById("bng").value);
    let sub3 = parseInt(document.getElementById("math").value);
    let sub4 = parseInt(document.getElementById("com").value);

    if (sub1>100 || sub2>100 || sub3>100 || sub4>100 ) {
        alert("Please Enter mark in range of 100");
    }
    
    else {
        let total = sub1 + sub2 + sub3 + sub4;
        let ave = total/4;
        document.getElementById("average").innerHTML = "<b> Your Average Marks is :  <b>"+ave;
    }
}


function Grade() {

    let sub1 = parseInt(document.getElementById("eng").value);
    let sub2 = parseInt(document.getElementById("bng").value);
    let sub3 = parseInt(document.getElementById("math").value);
    let sub4 = parseInt(document.getElementById("com").value);

    if (sub1>100 || sub2>100 || sub3>100 || sub4>100 ) {
        alert("Please Enter mark in range of 100");
    }
    
    else {
        let total = sub1 + sub2 + sub3 + sub4;
        let ave = total/4;

        if(ave>=80 && ave<=100){
            document.getElementById("grade").innerHTML = "<b>Your Grading point is : A<b>";
        }

        else if(ave>=60 && ave<=75){
            document.getElementById("grade").innerHTML = "<b>Your Grading point is : B<b>";
        }

        else if(ave>=55 && ave<=65){
            document.getElementById("grade").innerHTML = "<b>Your Grading point is : C<b>";
        }

        else if(ave>=33 && ave<=55){
            document.getElementById("grade").innerHTML = "<b>Your Grading point is : D<b>";
        }

        if(ave>=0 && ave<=32){
            document.getElementById("grade").innerHTML = "<b>Your Grading point is : Fail<b>";
        }
    }
}

function Clear() {
    // let ClearAll = ["eng","bng","math","com"];

    document.getElementById("eng","bng","math","com").value = "";
}

